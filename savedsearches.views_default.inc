<?php

/**
 * Implementation of Views' hook_views_default_views().
 *
 * Provides a view that lists all saved searches as an administration interface.
 */
function savedsearches_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'savedsearches_admin';
  $view->description = 'Saved Searches Administration interface';
  $view->tag = 'savedsearches';
  $view->view_php = '';
  $view->base_table = 'savedsearches';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'uid' => array(
      'label' => 'User',
      'required' => 0,
      'id' => 'uid',
      'table' => 'savedsearches',
      'field' => 'uid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'short_name' => array(
      'label' => 'Short name',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'savedsearches_render' => 'plain',
      'exclude' => 0,
      'id' => 'short_name',
      'table' => 'savedsearches',
      'field' => 'short_name',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Owner',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'uid',
    ),
    'short_name_1' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'savedsearches_render' => 'link',
      'savedsearches_link_text' => 'view',
      'exclude' => 0,
      'id' => 'short_name_1',
      'table' => 'savedsearches',
      'field' => 'short_name',
      'relationship' => 'none',
    ),
    'admin_edit' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'savedsearches_link_text' => 'edit',
      'exclude' => 0,
      'id' => 'admin_edit',
      'table' => 'savedsearches',
      'field' => 'admin_edit',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'short_name' => array(
      'order' => 'ASC',
      'id' => 'short_name',
      'table' => 'savedsearches',
      'field' => 'short_name',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'uid' => array(
      'operator' => 'in',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'uid_op',
        'identifier' => 'uid',
        'label' => 'User name',
        'optional' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'uid',
    ),
    'search_path' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'search_path_op',
        'identifier' => 'search_path',
        'label' => 'Search path contains',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'search_path',
      'table' => 'savedsearches',
      'field' => 'search_path',
      'relationship' => 'none',
    ),
    'short_name' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'short_name_op',
        'identifier' => 'short_name',
        'label' => 'Short name contains',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'short_name',
      'table' => 'savedsearches',
      'field' => 'short_name',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'administer saved searches',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Administer Saved Searches');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'short_name' => 'short_name',
      'name' => 'name',
      'short_name_1' => 'short_name_1',
      'admin_edit' => 'short_name_1',
    ),
    'info' => array(
      'short_name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'short_name_1' => array(
        'sortable' => 0,
        'separator' => ', ',
      ),
      'admin_edit' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/content/savedsearches');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Saved searches',
    'description' => 'Edit and delete saved searches.',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $views[$view->name] = $view;
  unset($view);

  $view = new view;
  $view->name = 'savedsearches_user';
  $view->description = 'A list of the current user\'s saved searches.';
  $view->tag = 'savedsearches';
  $view->view_php = '';
  $view->base_table = 'savedsearches';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'uid' => array(
      'label' => 'User',
      'required' => 1,
      'id' => 'uid',
      'table' => 'savedsearches',
      'field' => 'uid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'short_name' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'savedsearches_render' => 'link',
      'savedsearches_link_text' => '',
      'exclude' => 0,
      'id' => 'short_name',
      'table' => 'savedsearches',
      'field' => 'short_name',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'uid_current' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'uid_current',
      'table' => 'users',
      'field' => 'uid_current',
      'relationship' => 'uid',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'create saved searches',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'My Saved Searches');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', 'mini');
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'type' => 'ul',
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $views[$view->name] = $view;
  unset($view);

  return $views;
}