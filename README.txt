
A few notes:

* This module requires Views, since the Saved Searches user search listing and admin listing both rely on it.

* This module relies on the "search content" permission to determine whether users are allowed to view saved search pages.

----
This module is developed by Palantir.net (http://palantir.net/).
