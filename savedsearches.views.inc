<?php

/**
 * Implementation of Views' hook_views_handlers().
 *
 * Provide field handlers to generate our own links.
 */
function savedsearches_views_handlers() {
  return array(
    'handlers' => array(
      'savedsearches_handler_field_link' => array(
        'parent' => 'views_handler_field',
      ),
      'savedsearches_handler_field_admin_edit' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of Views' hook_views_data().
 */
function savedsearches_views_data() {
  $data = array();

  $data['savedsearches']['table']['group'] = 'Saved Searches';
  $data['savedsearches']['table']['base'] = array(
    'field' => 'ssid',
    'title' => t('Saved searches'),
    'help' => t("Users' saved searches."),
  );
  $data['savedsearches']['ssid'] = array(
    'title' => 'Saved search ID',
    'help' => 'The unique id for a saved search.',
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['savedsearches']['short_name'] = array(
    'title' => 'Short name',
    'help' => 'The URL slug for a saved search.',
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'field' => array(
      'handler' => 'savedsearches_handler_field_link',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['savedsearches']['search_path'] = array(
    'title' => 'Search path',
    'help' => 'The Drupal path to a set of search results, starting after "search/".',
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['savedsearches']['search_filters'] = array(
    'title' => 'Search filters',
    'help' => 'The Apache Solr filter query for a set of search results.',
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['savedsearches']['uid'] = array(
    'title' => 'User',
    'help' => 'The user ID of the owner of this saved search.',
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
  );
  $data['savedsearches']['admin_edit'] = array(
    'title' => 'Administration link',
    'help' => "A link to the saved search administrators' edit form.",
    'real field' => 'ssid',
    'field' => array(
      'handler' => 'savedsearches_handler_field_admin_edit',
    ),
  );

  return $data;
}