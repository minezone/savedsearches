<?php

/**
 * Simple field handler for saved searches that checks permissions and provides an administration
 * link.
 */
class savedsearches_handler_field_admin_edit extends views_handler_field {
  function access() {
    return user_access('administer saved searches');
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['savedsearches_link_text'] = array('default' => 'edit');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['savedsearches_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->options['savedsearches_link_text'],
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    $link_text = $this->options['savedsearches_link_text'];
    return l($link_text, 'admin/content/savedsearches/edit/' . $value);
  }
}