<?php

/**
 * Simple field handler for saved searches that checks permissions and provides a link to the saved
 * searches path.
 */
class savedsearches_handler_field_link extends views_handler_field {
  function access() {
    return user_access('search content');
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['savedsearches_render'] = array('default' => 'link');
    $options['savedsearches_link_text'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['savedsearches_render'] = array(
      '#type' => 'select',
      '#title' => t('Saved searches render style'),
      '#options' => array('link' => 'Link to saved search', 'plain' => 'Plain text'),
      '#default_value' => $this->options['savedsearches_render'],
    );
    $form['savedsearches_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#description' => t('Leave this blank to use the short name itself as link text.'),
      '#default_value' => $this->options['savedsearches_link_text'],
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    switch ($this->options['savedsearches_render']) {
      case 'link':
        $link_text = $this->options['savedsearches_link_text'] ? $this->options['savedsearches_link_text'] : $value;
        return l($link_text, 's/' . $value);
      case 'plain':
        return $value;
    }
  }
}